#!/bin/bash
# Condense and merge time sperated data files
# ===========================================
# Example of using CDO to do time averaging and merging data files
# containing the same meteorological parameters with respect to time.

datadir=/home/ubuntu/era5
metprms=(northward_wind_at_10_metres eastward_wind_at_10_metres)

cd $datadir
for metprm in ${metprms[@]}  ; do
    dfiles=$( ls ${metprm}_*.nc )   
    for dfile in ${dfiles[@]} ; do                      
	cdo monmean ${dfile} monmean_${dfile}.nc
    done
    dfiles=$( ls monmean_${metprm}_*.nc )
    cdo mergetime ${dfiles[@]} ${metprm}_monmean_era5.nc
done




