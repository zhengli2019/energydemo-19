''' Spot Check Data Files
    ---------------------
    Outline a script to run quick checks on contents of individual files
    aquired through provided downloading scripts. 

    ** Note: Tries to flag possible missing or mismathced values, but only
             based off of consistancey with a given file name, not on meta
             information about what a given user might 'want' to have.    '''

import glob as grod
import pandas as pd
import numpy as np
import os
from cdo import *
cdo = Cdo()

datadir = '/home/ubuntu/eucordex'        # set folder to examine

# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====

''' Find data files
    ---------------
    Go to the above specified directory and list out the CORDEX files 
    contained within. Do this using wildcards (*) with the assumption that
    the files are in NetCDF format (.nc) and have been downloaded from 
    the Euro CORDEX database (EUR-11).                                     '''

os.chdir(datadir)                         
filenames = grod.glob('*_EUR-11_*.nc')

''' Check name/content matches
    --------------------------
    * Run through the files and check that the file name label matches the 
      data labels within a given file. Assumes one parameter per file.   
    * Read the time stamps within a file, and check that they match the 
      filename labels. Assumes naming standard that date is last name 
      element. Since some of the CORDEX simulations use irregular calendars
      can't check day to day, so as a first pass check that full sequence
      of years is present.                                               
    * Check that the files are of a minimal size.
    * Record labels components for additional checking.                '''


metadata = pd.DataFrame(
    index=range(len(filenames)),
    columns=['metprm','region','gcm','scenario','ver_gcm',
             'rcm','ver_rcm','timestep','daterange'])
ix = 0
for filename in filenames :
    
    ## Check names 
    label = filename.split('_')[0]
    content = cdo.showvar(input=filename)[0]
    if label != content :
        print("+++ MetPrm label mismatch! +++ ")
        print(filename+ " contains parameter "+content)
        print("")
    # else : print(filename+" checks out")
    
    ## Check years 
    yr0 = int(filename.split('_')[-1].split('.')[0].split('-')[0][:4])
    yr1 = int(filename.split('_')[-1].split('.')[0].split('-')[1][:4])
    label = list(range(yr0,yr1+1)) 
    content = [int(x) for x in cdo.showyear(input=filename)[0].split(' ')]
    diff = list(set(label).difference(content))
    if diff :
        print("+++ Year mismatch! +++ ")
        content = ', '.join([str(x) for x in content])
        print(filename+ " contains years: "+content)
        print("")
    # else : print(filename+ ": years match!")

    ## Check file size
    if os.path.getsize(filename) < 5000000 :
        print("+++ Possible dud file +++ ")
        print(filename + " is suspiciously small")
        print("")

    ## Record metadata
    labels = filename.split('_')
    labels[-1] = labels[-1].split('.')[0]
    metadata.loc[ix,:] = labels

    ix += 1

''' Write out meta labels
    ---------------------
    Save a file of file name labels so that can check other matches.       '''

metadata.to_csv('filename_labels.csv')

# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====
# ////////////////////////////////////////////////////////////////////////////

''' Can use the metadata table to check things such as which sims are 
    available for which parameters. For example...                         '''

metadata = pd.read_csv('filename_labels.csv')       # read list

uas_sims = [                                        # find simulation pairs
    '-'.join(x)                                     #   associated with 
    for x in metadata.loc[                          #   different parameters
            metadata.metprm=='uas',[
                'gcm','rcm']].values]
uas_sims = np.unique(uas_sims)
vas_sims = [
    '-'.join(x)
    for x in metadata.loc[
            metadata.metprm=='vas',[
                'gcm','rcm']].values]
vas_sims = np.unique(vas_sims)
swm_sims = [
    '-'.join(x)
    for x in metadata.loc[
            metadata.metprm=='sfcWindmax',[
                'gcm','rcm']].values]
swm_sims = np.unique(swm_sims)

diff = list(set(uas_sims).difference(vas_sims))    # compare simulation lists
if diff :
    print("+++ UAS and VAS don't have matching simulation sets! +++")
    print("")

diff = list(set(uas_sims).difference(swm_sims))    
if diff :
    print("+++ Simulations of UAS but not MaxWindSpeed +++")
    print(diff)

diff = list(set(swm_sims).difference(uas_sims))    
if diff :
    print("+++ Simulations of MaxWindSpeed but not UAS +++")
    print(diff)

# ////////////////////////////////////////////////////////////////////////////
# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====
